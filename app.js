
var http = require('http');
var path = require('path');
var express = require('express');
var routes = require('./routes');
var comic = require('./routes/comics.js');
var character = require('./routes/characters.js');
var app = express();

app.engine('.html', require('ejs').__express);

app.set('port', 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'html');
app.use(express.static(__dirname + '/public'));

app.get('/', routes.index);
app.get('/comics', comic.comics);
app.get('/characters', character.characters);

http.createServer(app).listen(app.get('port'));

console.log("listening in localhost 3000");

