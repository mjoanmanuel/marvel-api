/** comics page **/

var utils = require('../utils.js');

exports.characters = function (req, res) {
  console.log('request characters');
  utils.characters(function(data){
    res.render('characters', {
      title: 'Marvel Characters',
      characters: data
    });
  });
};
