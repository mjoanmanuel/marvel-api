/** comics page **/

var utils = require('../utils.js');

exports.comics = function (req, res) {
  console.log('request comics');
  
  utils.comics(function(data) {
    var props = utils.generateHash();
    res.render('comics', {
      title: 'Marvel Comics',
      comics: data
    });
  });
};
