/** utils module for Marvel API **/
var crypto = require('crypto');
var request = require('request');
var dateFormat = require('dateformat');
var PRIVATE_KEY = "mineisbetterthanyours";
var PUBLIC_KEY = "4781115c196c14430082ed3decfe3d34";

function generateHash() {
  var ts = dateFormat(new Date(), 'dd-mm-yyyy');
  var string = ts + PRIVATE_KEY + PUBLIC_KEY;
  var hash = crypto.createHash("md5").update(string).digest("hex");
  var result = {"ts": ts, "hash": hash};
  return result;
}

function comics(callback) {
  return api('comics', callback);
}

function characters(callback) {
  return api('characters', callback);
}

function api(operation, callback) {
  console.log('requesting operation ' + operation);
  var props = generateHash();
  var url = 'http://gateway.marvel.com/v1/public/' +operation + '?apikey=' + PUBLIC_KEY + '&ts=' + props.ts + '&hash=' + props.hash;
  var req = request(url, function(error, response, body){
    if(error) console.log(error);
    else {
     console.log('request succesfully sent.');
     var json = JSON.parse(body);
     callback(JSON.stringify(json.data.results));
    }
  });
}

exports.publicKey = PUBLIC_KEY;
exports.generateHash = generateHash;
exports.comics = comics;
exports.characters = characters;

